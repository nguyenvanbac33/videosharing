<div class="col-sm-3 col-md-2 sidebar">
	<div class="top-navigation">
		<div class="t-menu">MENU</div>
		<div class="t-img">
			<img src="images/lines.png" alt="" />
		</div>
		<div class="clearfix"> </div>
	</div>
	<div class="drop-navigation drop-navigation">
		<ul class="nav nav-sidebar">
			<li class="active"><a href="index.php" class="home-icon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<!-- script-for-menu -->
			<script>
				$( "li a.menu1" ).click(function() {
					$( "ul.cl-effect-2" ).slideToggle( 300, function() {
							// Animation complete.
						});
				});
			</script>
			<?php
				if (isset($_SESSION['user'])) {
			?>
			<li><a href="favorite.php" class="sub-icon"><span class="glyphicon glyphicon-home glyphicon-hourglass" aria-hidden="true"></span>Favorited</a></li>
			<li><a href="#" class="menu"><span class="glyphicon glyphicon-film glyphicon-king" aria-hidden="true"></span>My Channel<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-1">
				<li><a href="upload.php">Upload</a></li>                                             
				<li><a href="my-video.php">My Video</a></li>
			</ul>
			<?php
				}
			?>
			<!-- script-for-menu -->
			<script>
				$( "li a.menu" ).click(function() {
					$( "ul.cl-effect-1" ).slideToggle( 300, function() {
							// Animation complete.
						});
				});
			</script>
			<li><a href="?logout=1" class="news-icon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Logout</a></li>
		</ul>
		<!-- script-for-menu -->
		<script>
			$( ".top-navigation" ).click(function() {
				$( ".drop-navigation" ).slideToggle( 300, function() {
							// Animation complete.
						});
			});
		</script>
	</div>
</div>