<?php
session_start();
$host = "localhost";
$user = "root";
$password = "";
$dbName = "video_sharing";
$conn = mysqli_connect($host,$user,$password,$dbName);
mysqli_set_charset($conn,"utf8");

function getCurrentDate(){
	return date("d/m/Y");
}

function duration($seconds_count)
{
	$delimiter  = ':';
	$seconds = $seconds_count % 60;
	$minutes = floor($seconds_count/60);
	$hours   = floor($seconds_count/3600);

	$seconds = str_pad($seconds, 2, "0", STR_PAD_LEFT);
	$minutes = str_pad($minutes, 2, "0", STR_PAD_LEFT).$delimiter;

	if($hours > 0)
	{
		$hours = str_pad($hours, 2, "0", STR_PAD_LEFT).$delimiter;
	}
	else
	{
		$hours = '';
	}

	return "$hours$minutes$seconds";
}

function formatViews($views) {
	$symbol_thousand = '.';
	$decimal_place = 0;
	$price = number_format($views, $decimal_place, '', $symbol_thousand);
	return $price;
}
?>