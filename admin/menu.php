<li class="nav-item <?php if($page == 'user') echo 'active' ?>">
    <a class="nav-link" href="?page=user">
	    <i class="material-icons">person</i>
	   	<p>Quản trị</p>
    </a>
</li>
<li class="nav-item <?php if($page == 'guess') echo 'active' ?>">
	<a class="nav-link" href="?page=guess">
		<i class="material-icons">group</i>
		<p>Khách hàng</p>
	</a>
</li>
<li class="nav-item <?php if($page == 'group') echo 'active' ?>">
	<a class="nav-link" href="?page=group">
		<i class="material-icons">group</i>
		<p>Group</p>
	</a>
</li>
<li class="nav-item">
	<a class="nav-link" href="../admin/login.php?logout=logout">
		<i class="material-icons">exit_to_app</i>
		<p>Đăng xuất</p>
	</a>
</li>