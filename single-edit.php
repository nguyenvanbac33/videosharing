
<?php
$id = $_GET['id'];
if (isset($_POST['update-video'])) {
	$name = $_POST['name'];
	$desc = $_POST['desc'];
	$group = $_POST['group'];
	$sql = "UPDATE `video` SET `name`='$name',`description`='$desc',`id_group`=$group WHERE `id`=$id";
	$result = $conn -> query($sql);
	if ($result) {
		echo "<script type='text/javascript'>alert('Update success');</script>";
		echo '<meta http-equiv="refresh" content="0">';
	}else{
		echo "<script type='text/javascript'>alert('Update fail');</script>";
	}
} else if (isset($_GET['delete'])) {
	$sql = "DELETE FROM `video` WHERE id = $id";
	$result = $conn->query($sql);
	if($result){
		echo "<script type='text/javascript'>alert('Delete success');</script>";
		echo "<script>location.href='my-video.php';</script>";
	}else{
		echo "<script type='text/javascript'>alert('Delete fail');</script>";
	}
} else if (isset($_GET['suspend'])) {
	$status = $_GET['suspend'];
	$s = 0;
	if ($status == 0) {
		$s = 1;
	}
	$sql = "UPDATE video set status = $s where id = $id";
	$conn -> query($sql);
	echo "<script>location.href='single.php?id=$id';</script>";
}
$sql = "SELECT * FROM video where id = $id";
$query = $conn -> query($sql);
$rowV = $query -> fetch_array();
?>
<div class="signin">
	<?php
	$e = $_SESSION['user']['email'];
	if ($e == $row['user_id'] || $_SESSION['user']['type'] == 1) {
		?>
		<div>
			<?php
			if ($e == $row['user_id']) {
				?>
				<a href="#small-dialog4" class="play-icon popup-with-zoom-anim">Edit</a>
				<a href="?id=<?php echo $id?>&delete=1" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
				<?php
			}
			if ($_SESSION['user']['type'] == 1) {
				?>
				<a href="?id=<?php echo $id?>&suspend=<?php echo $row['status']?>" onclick="return confirm('Are you sure?');">
					<?php 
					if ($row['status'] == 0) {
						echo "Suspend";
					} else {
						echo "Active";
					}?>
				</a>
				<?php
			}
			?>
		</div>
		<?php
	}
	?>
	<div class="clearfix"> </div>
	<div id="small-dialog4" style="width: 30%" class="mfp-hide">
		<h3>Update Video</h3> 
		<div class="signup">
			<form method="post" id="fr-edit">
				<div class="signup">
					<input type="text" name="name" value="<?php echo $rowV['name']?>" placeholder="Video name" required/>
					<select name="group">
						<?php
						$sql = "SELECT * FROM video_group";
						$query = $conn -> query($sql);
						while ($r = $query -> fetch_array()) {
							?>
							<option <?php if ($r['id'] == $rowV['id_group']) {echo "selected";}?> value="<?php echo $r['id']?>"><?php echo $r['name']?></option>
							<?php
						}
						?>
					</select>
					<textarea name="desc" form="fr-edit" placeholder="Video description" rows="5"><?php echo $rowV['description']?></textarea>
					<input type="submit" name="update-video"  value="UPDATE"/>
				</div>
			</form>
		</div>
		<div class="clearfix"> </div>
	</div>	
</div>
<div class="clearfix"> </div>