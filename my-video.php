<?php
include 'header.php';
include 'menu.php';
if (isset($_GET['author'])) {
	$email = $_GET['author'];
} else {
	$email = $_SESSION['user']['email'];
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<?php include 'add-playlist.php';?>
			<div class="recommended-info">
				<h3>Playlist</h3>
			</div>
			<?php
			$sql = "SELECT * FROM playlist where user_id = '$email'";
			$query = $conn -> query($sql);
			while ($row = $query -> fetch_array()) {
				$sql = "SELECT * FROM playlist_detail a inner join video b on a.id_video = b.id where a.id_playlist = ". $row['id'];
				$q = $conn -> query($sql);
				$count = mysqli_num_rows($q);
				$img = "images/avatar/no_products_found.png";
				if ($count > 0) {
					$r = $q -> fetch_array();
					$img = $r['thub'];
				}
				?>
				<div class="col-md-4 resent-grid recommended-grid slider-top-grids">
					<div class="resent-grid-img recommended-grid-img" style="background: #000">
						<a href="playlist.php?id=<?php echo $row['id']?>"><img height="300" src="<?php echo $img?>" alt="" /></a>
						<div class="time">
							<p><?php echo $count." Videos"?></p>
						</div>
					</div>
					<div class="resent-grid-info recommended-grid-info">
						<h3><a href="playlist.php?id=<?php echo $row['id']?>" class="title title-info"><?php echo $row['name']?></a></h3>
					</div>
				</div>
				<?php
			}
			?>
			<div class="clearfix"> </div>
		</div>
		<div class="recommended">
			<div class="recommended">
				<div class="recommended-grids">
					<div class="recommended-info">
						<h3>All Video</h3>
					</div>
					<?php
					$sql = "SELECT * FROM video where user_id = '$email'";
					$results = $conn -> query($sql);
					while ($row = $results -> fetch_array()) {
						?>
						<div class="col-md-3 resent-grid recommended-grid"  style="margin-top: 20px">
							<div class="resent-grid-img recommended-grid-img">
								<a href="single.php?id=<?php echo $row['id']?>"><img height="200" src="<?php echo $row['thub']?>" alt="" /></a>
								<div class="time small-time">
									<p><?php echo duration($row['duration'])?></p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="single.php?id=<?php echo $row['id']?>" class="title"><?php echo $row['name']?></a></h5>
								<ul>
									<li><p class="author author-info"><?php echo $row['pub_date']?></p></li>
									<li class="right-list"><p class="views views-info"><?php echo formatViews($row['views'])?> views</p></li>
								</ul>
							</div>
						</div>
						<?php
					}
					?>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
<?php
include 'footer.php';
?>