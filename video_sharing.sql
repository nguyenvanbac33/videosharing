-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2020 at 06:22 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `video_sharing`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `email` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`email`, `name`, `phone`, `password`, `type`) VALUES
('admin@gmail.com', 'Admin', '0123456780', 'admin', 1),
('t@gmail.com', 'Test', '0123456789', '1', 0),
('test@gmail.com', 'Nguyễn Văn A', '012345670', '123456', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `video_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `pub_date` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `video_id`, `content`, `pub_date`) VALUES
(1, 'test@gmail.com', 6, 'Hya', '30/05/2020'),
(2, 't@gmail.com', 7, 'Hay', '30/05/2020'),
(3, 't@gmail.com', 7, 'Hay', '30/05/2020'),
(4, 't@gmail.com', 10, 'Video đẹp', '30/05/2020');

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id_user` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_video` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id_user`, `id_video`) VALUES
('test@gmail.com', 7),
('t@gmail.com', 7);

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `user_id` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`id`, `user_id`, `name`) VALUES
(2, 'test@gmail.com', 'Thiên nhiên'),
(6, 'test@gmail.com', 'Tình Yêu'),
(7, 't@gmail.com', 'Effect');

-- --------------------------------------------------------

--
-- Table structure for table `playlist_detail`
--

CREATE TABLE `playlist_detail` (
  `id_playlist` int(11) NOT NULL,
  `id_video` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `playlist_detail`
--

INSERT INTO `playlist_detail` (`id_playlist`, `id_video`) VALUES
(2, 6),
(2, 7),
(7, 9),
(7, 10),
(7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `thub` text COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) NOT NULL,
  `user_id` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `pub_date` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `id_group` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `url`, `thub`, `views`, `user_id`, `duration`, `pub_date`, `description`, `id_group`, `status`) VALUES
(6, 'Thiên nhiên sinh động', 'images/video/Relaxing Nature Screensaver http---www.screensavergift.com.mp4', 'images/thub/device-2020-05-07-225309.png', 10, 'test@gmail.com', 50, '03/05/2020', '', 2, 0),
(7, 'White Christmas 3D Live Wallpaper and Screensaver', 'images/video/White Christmas 3D Live Wallpaper and Screensaver.mp4', 'images/thub/172552_05032012_254962.jpg', 93, 'test@gmail.com', 60, '30/05/2020', 'dsadsadsa', 4, 1),
(9, 'Count down time', 'images/video/Demnguoc.mp4', 'images/thub/Capture.PNG', 2, 't@gmail.com', 26, '30/05/2020', 'Count down time for game', 1, 0),
(10, 'Animate for video slide show nature', 'images/video/v1.mp4', 'images/thub/3.PNG', 2, 't@gmail.com', 202, '30/05/2020', '', 4, 0),
(11, 'Intro video star', 'images/video/intro1.mp4', 'images/thub/2.PNG', 1, 't@gmail.com', 11, '30/05/2020', '', 4, 0),
(12, 'Bắn pháo hoa giao thừa Việt Nam', 'images/video/phaohoa.mp4', 'images/thub/4.PNG', 0, 't@gmail.com', 539, '30/05/2020', '', 4, 0),
(13, 'Trái tim hồng', 'images/video/hai vãi.mp4', 'images/thub/333.PNG', 0, 't@gmail.com', 30, '30/05/2020', '', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `video_group`
--

CREATE TABLE `video_group` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video_group`
--

INSERT INTO `video_group` (`id`, `name`) VALUES
(1, 'Game show'),
(2, 'Kids'),
(3, 'Sport'),
(4, 'New Movie');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_group`
--
ALTER TABLE `video_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `video_group`
--
ALTER TABLE `video_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
