<?php
include 'header.php';
if (isset($_POST['upload'])) {
	$email = $_SESSION['user']['email'];
	$name = $_POST['name'];
	$duration = $_POST['duration'];
	$date = getCurrentDate();
	$desc = $_POST['desc'];
	$group = $_POST['group'];
	// thub
	$filename = $_FILES['thub']['name'];
	$thub = "images/thub/" . $filename;
	move_uploaded_file($_FILES['thub']['tmp_name'], $thub);
	// video
	$filename = $_FILES['video']['name'];
	$video = "images/video/" . $filename;
	move_uploaded_file($_FILES['video']['tmp_name'], $video);
	$sql = "INSERT INTO `video`(`name`, `url`, `thub`, `views`, `user_id`, `duration`, pub_date, description, id_group) VALUES ('$name', '$video', '$thub', 0, '$email', $duration, '$date', '$desc', $group)";
	$result = $conn -> query($sql);
	if ($result == '') {
		echo "<script type='text/javascript'>alert('Upload fail');</script>";
	}else{
		echo "<script type='text/javascript'>alert('Upload success');</script>";
		echo '<meta http-equiv="refresh" content="0">';
	}
}
?>
<!-- upload -->
<div class="upload">
	<!-- container -->
	<div class="container">
		<form method="post" enctype="multipart/form-data" id="fr-upload">
			<div class="upload-grids">
				<div class="upload-right col-md-12">
					<video class="upload-right col-md-6" width="100%" controls id="video" style="visibility: hidden; position: absolute; z-index: 2">
					</video>
					<div class="upload-right col-md-6" style="height: 100%">
						<div id="chooser">
							<div class="upload-file">
								<div class="services-icon">
									<span class="glyphicon glyphicon-open" aria-hidden="true"></span>
								</div>
								<input type="file" onchange="readURL(this);" accept="video/*" name="video" style="left: 44%; top: 30%" value="Choose file.." required>
							</div>
							<div class="upload-info">
								<h5>Select files to upload</h5>
							</div>
						</div>
					</div>
					<div class="upload-right col-md-6" style="border-left: 1px; border-color: #000">
						<div class="signup">
							<input type="text" name="name" placeholder="Video name" required/>
							<input type="text" id="duration" name="duration" placeholder="Video duration" readonly required/>
							<select name="group">
								<?php
									$sql = "SELECT * FROM video_group";
									$query = $conn -> query($sql);
									while ($row = $query -> fetch_array()) {
										?>
											<option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
										<?php
									}
								?>
							</select>
							<textarea name="desc" form="fr-upload" placeholder="Video description" rows="5"></textarea>
							<input type="file" name="thub" accept="image/*" style="margin-left: 5%" required/>
							<input type="submit" name="upload"  value="UPLOAD"/>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	<!-- //container -->
</div>
<!-- //upload -->
<?php
include 'footer.php';
?>

<script type="text/javascript">
	var vid = document.getElementById("video");
	vid.addEventListener('loadeddata', function() {
		$('#duration').attr('value', vid.duration);
	}, false);
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#video')
				.attr('src', e.target.result)
				.attr('style', "visibility: visible; padding: 10px; position: absolute; z-index: 2; top: 29%");

				$('#chooser')
				.attr('style', "visibility: hidden");
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>