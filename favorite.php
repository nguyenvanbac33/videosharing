<?php
include 'header.php';
include 'menu.php';
$email = $_SESSION['user']['email'];
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="recommended">
			<div class="recommended">
				<div class="recommended-grids">
					<div class="recommended-info">
						<h3>Favorited</h3>
					</div>
					<?php
					$sql = "SELECT a.* FROM video a inner join favorite b on a.id = b.id_video where b.id_user = '$email'";
					$results = $conn -> query($sql);
					while ($row = $results -> fetch_array()) {
						?>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="single.php?id=<?php echo $row['id']?>"><img height="200" src="<?php echo $row['thub']?>" alt="" /></a>
								<div class="time small-time">
									<p><?php echo duration($row['duration'])?></p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="single.php?id=<?php echo $row['id']?>" class="title"><?php echo $row['name']?></a></h5>
								<ul>
									<li><p class="author author-info"><?php echo $row['pub_date']?></p></li>
									<li class="right-list"><p class="views views-info"><?php echo formatViews($row['views'])?> views</p></li>
								</ul>
							</div>
						</div>
						<?php
					}
					?>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<?php
		include 'footer.php';
		?>