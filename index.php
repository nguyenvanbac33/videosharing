<?php
include 'header.php';
include 'menu.php';
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<div class="recommended-info">
				<h3>Most Videos</h3>
			</div>
			<?php
			$sql = "SELECT a.*, b.name as author FROM video a inner join account b on a.user_id = b.email where a.name like '%$key%' ORDER BY views desc limit 3";
			$query = $conn -> query($sql);
			while ($row = $query -> fetch_array()) {
				?>
				<div class="col-md-4 resent-grid recommended-grid slider-top-grids">
					<div class="resent-grid-img recommended-grid-img">
						<a href="single.php?id=<?php echo $row['id']?>"><img height="300" src="<?php echo $row['thub']?>" alt="" /></a>
						<div class="time">
							<p><?php echo duration($row['duration'])?></p>
						</div>
						<div class="clck">
							<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
						</div>
					</div>
					<div class="resent-grid-info recommended-grid-info">
						<h3><a href="single.php?id=<?php echo $row['id']?>" class="title title-info"><?php echo $row['name']?></a></h3>
						<ul>
							<li><p class="author author-info"><a href="my-video.php?author=<?php echo $row['user_id']?>" class="author"><?php echo $row['author']?></a></p></li>
							<li class="right-list"><p class="views views-info"><?php echo formatViews($row['views'])?> views</p></li>
						</ul>
					</div>
				</div>
				<?php
			}
			?>
			<div class="clearfix"> </div>
		</div>
		<div class="recommended">
			<?php
			$sql = "SELECT * FROM video_group";
			$query = $conn -> query($sql);
			while ($row = $query -> fetch_array()) {
				$sql = "SELECT a.*, b.name as author FROM video a inner join account b on a.user_id = b.email where a.name like '%$key%' and id_group = ". $row['id'];
				$q = $conn -> query($sql);
				$num_rows = mysqli_num_rows($q);
				if ($num_rows > 0) {
					?>
					<div class="recommended-grids">
						<div class="recommended-info">
							<h3><?php echo $row['name']?></h3>
						</div>
						<?php
						while ($r = $q -> fetch_array()) {
							?>
							<div class="col-md-3 resent-grid recommended-grid" style="margin-top: 20px">
								<div class="resent-grid-img recommended-grid-img">
									<a href="single.php?id=<?php echo $r['id']?>"><img height="200" src="<?php echo $r['thub']?>" alt="" /></a>
									<div class="time small-time">
										<p><?php echo duration($r['duration'])?></p>
									</div>
									<div class="clck small-clck">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div class="resent-grid-info recommended-grid-info video-info-grid">
									<h5><a href="single.php?id=<?php echo $r['id']?>" class="title"><?php echo $r['name']?></a></h5>
									<ul>
										<li><p class="author author-info"><a href="my-video.php?author=<?php echo $r['user_id']?>" class="author"><?php echo $r['author']?></a></p></li>
										<li class="right-list"><p class="views views-info"><?php echo formatViews($r['views'])?> views</p></li>
									</ul>
								</div>
							</div>
							<?php
						}
						?>
						<div class="clearfix"> </div>
					</div>
					<?php
				}
			}
			?>
		</div>
	</div>
	<?php
	include 'footer.php';
	?>