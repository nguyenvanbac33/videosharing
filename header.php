<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
include 'connection.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>My Play a Entertainment Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap -->
	<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />
	<!-- //bootstrap -->
	<link href="css/dashboard.css" rel="stylesheet">
	<!-- Custom Theme files -->
	<link href="css/style.css" rel='stylesheet' type='text/css' media="all" />
	<script src="js/jquery-1.11.1.min.js"></script>
	<!--start-smoth-scrolling-->
	<!-- fonts -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
	<!-- //fonts -->
	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>    
	<link href="css/popuo-box.css?version=1.2" rel="stylesheet" type="text/css" media="all" />
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
</head>
<?php
	if (isset($_GET['logout'])) {
		unset($_SESSION['user']);
		echo "<script>location.href='index.php';</script>";
	}
	$key = "";
	if (isset($_POST['key-search'])) {
		$key = $_POST['key-search'];
	}
?>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><h1><img src="images/logo.png" alt="" /></h1></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<div class="top-search">
					<form class="navbar-form navbar-right" method="post">
						<input type="text" name="key-search" value="<?php echo $key?>" class="form-control" placeholder="Search...">
						<input type="submit" value=" ">
					</form>
				</div>
				<div class="header-top-right">
					<?php
						if (!isset($_SESSION["user"])) {
							include 'register.php';
							include 'login.php'; 
						} else {
							include 'profile.php';
						}
					?>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</nav>
	<script>
		$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>	