<?php
include 'header.php';
include 'menu.php';
$id = $_GET['id'];
$conn -> query("UPDATE video set views = views + 1 where id = $id");
$sql = "SELECT * FROM video where id = $id";
$results = $conn -> query($sql);
$row = $results -> fetch_array();
if (isset($_POST['send'])) {
	$content = $_POST['content'];
	$user = $_SESSION['user']['email'];
	$date = getCurrentDate();
	$sql = "INSERT INTO `comment`(`user_id`, `video_id`, `content`, `pub_date`) VALUES ('$user', $id, '$content', '$date')";
	$conn -> query($sql);
} else if (isset($_GET['unfavorite'])) {
	$user = $_SESSION['user']['email'];
	$sql = "DELETE FROM `favorite` WHERE id_video = $id and id_user = '$user'";
	$conn -> query($sql);
	echo "<script>location.href='single.php?id=$id';</script>";
} else if (isset($_GET['favorite'])) {
	$user = $_SESSION['user']['email'];
	$sql = "INSERT INTO `favorite`(`id_user`, `id_video`) VALUES ('$user', $id)";
	$conn -> query($sql);
	echo "<script>location.href='single.php?id=$id';</script>";
}
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="col-sm-8 single-left">
			<div class="song">
				<div class="song-info">
					<h3>
						<?php
						if ($row['status'] == 1) {
							echo '[Suspended] '. $row['name'];
						} else {
							echo $row['name'];
						}
						?>
					</h3>	
				</div>
				<div class="video-grid">
					<video width="100%" controls 
					src="<?php
					if($row['status'] == 0) {
						echo $row['url'];
					}
					?>"></video>
				</div>
			</div>
			<div class="song-grid-right">
				<div class="share">
					<ul>
						<?php
						if (isset($_SESSION['user'])) {
							$user = $_SESSION['user']['email'];
							$sql = "SELECT * FROM favorite where id_user = '$user' and id_video = $id";
							$query = $conn -> query($sql);
							$rowFa = $query -> fetch_array();
							if ($rowFa) {
								?>
								<li><a href="?id=<?php echo $id?>&unfavorite=1" class="icon like">Unfavorite</a></li>
								<?php
							} else {
								?>
								<li><a href="?id=<?php echo $id?>&favorite=1" class="icon like">Favorite</a></li>
								<?php
							}
						}
						?>
						<li class="view"><?php echo $row['views']?> Views</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"> </div>
			<div><?php include 'single-edit.php';?></div>
			<div class="published">
				<h4>Published on <?php echo $row['pub_date']?></h4>
				<p><?php echo $row['description']?></p>
			</div>
			<div class="all-comments">
				<?php
				$sql = "SELECT * FROM comment a inner join account b on a.user_id = b.email where video_id = $id";
				$results = $conn -> query($sql);
				$num_rows = mysqli_num_rows($results);
				?>
				<div class="all-comments-info">
					<a href="#">All Comments (<?php echo $num_rows?>)</a>
					<?php
					if (isset($_SESSION['user'])) {
						?>
						<div class="box">
							<form method="post" id="fr-comment">
								<textarea placeholder="Message" form="fr-comment" name="content" required></textarea>
								<input type="submit" name="send" value="SEND">
								<div class="clearfix"> </div>
							</form>
						</div>
						<?php
					}
					?>
				</div>
				<div class="media-grids">
					<?php
					while ($rowC = $results -> fetch_array()) {
						?>
						<div class="media">
							<h5><?php echo $rowC['name']?></h5>
							<div class="media-left">
								<a href="my-video.php?author=<?php echo $rowC['email']?>">
									<img src="images/avatar/<?php echo $rowC['email']?>" onerror="this.onerror=null;this.src='images/avatar/default_user.png';" style="vertical-align: middle;width: 100%;height: 100%;border-radius: 50%;">
								</a>
							</div>
							<div class="media-body">
								<p><?php echo $rowC['content']?></p>
								<span><?php echo $rowC['pub_date']?></span>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
		<div class="col-md-4 single-right">
			<h3>Up Next</h3>
			<div class="single-grid-right">
				<?php
				$sql = "SELECT a.*, b.name as author FROM video a inner join account b on a.user_id = b.email where id <> $id and user_id = '".$row['user_id']."'";
				$results = $conn -> query($sql);
				while ($rowS = $results -> fetch_array()) {
					?>
					<div class="single-right-grids">
						<div class="col-md-4 single-right-grid-left">
							<a href="single.php?id=<?php echo $rowS['id']?>"><img src="<?php echo $rowS['thub']?>" alt="" /></a>
						</div>
						<div class="col-md-8 single-right-grid-right">
							<a href="single.php?id=<?php echo $rowS['id']?>" class="title"> <?php echo $rowS['name']?></a>
							<p class="author"><a href="my-video.php?author=<?php echo $rowS['user_id']?>" class="author"><?php echo $rowS['author']?></a></p>
							<p class="views"><?php echo formatViews($rowS['views'])?> views</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<?php include 'footer.php';?>