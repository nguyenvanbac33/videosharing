
<?php
$id = $_GET['id'];
if (isset($_POST['update-playlist'])) {
	$name = $_POST['name'];
	$sql = "UPDATE `playlist` SET `name`='$name' WHERE `id`='$id'";
	$result = $conn->query($sql);
	if($result){
		echo "<script type='text/javascript'>alert('Update success');</script>";
		echo '<meta http-equiv="refresh" content="0">';
	}else{
		echo "<script type='text/javascript'>alert('Update fail');</script>";
	}
} elseif (isset($_POST['update-video-list'])) {
	$email = $_SESSION['user']['email'];
	$sql = "SELECT * FROM video WHERE user_id = '$email'";
	$conn -> query("DELETE FROM playlist_detail where id_playlist = $id");
	$query = $conn -> query($sql);
	while ($row = $query -> fetch_array()) {
		$idVideo = $row['id'];
		if (isset($_POST[$idVideo])) {
			$sql = "INSERT INTO `playlist_detail`(`id_playlist`, `id_video`) VALUES ($id, $idVideo)";
			$conn -> query($sql);
		}
	}
	// echo "<script type='text/javascript'>alert('Update success');</script>";
	// echo '<meta http-equiv="refresh" content="0">';
} else if (isset($_GET['delete'])) {
	$sql = "DELETE FROM `playlist` WHERE id = $id";
	$result = $conn->query($sql);
	if($result){
		echo "<script type='text/javascript'>alert('Delete success');</script>";
		echo "<script>location.href='my-video.php';</script>";
	}else{
		echo "<script type='text/javascript'>alert('Delete fail');</script>";
	}
}
?>
<div class="signin">
	<?php
	$e = $_SESSION['user']['email'];
	if ($e == $rowP['user_id']) {
		?>
		<div style="position: absolute; z-index: 999; right: 20px;">
			<a href="#small-dialog4" class="play-icon popup-with-zoom-anim">Edit</a>
			<a href="#small-dialog5" class="play-icon popup-with-zoom-anim">Video</a>
			<a href="?id=<?php echo $id?>&delete=1" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
		</div>
		<?php
	}
	?>
	<div id="small-dialog4" style="width: 30%" class="mfp-hide">
		<h3>Update Playlist</h3> 
		<div class="signup">
			<form method="post">
				<input type="text" name="name" placeholder="Play list name" required="required"/>
				<input type="submit" name="update-playlist"  value="UPDATE"/>
			</form>
		</div>
		<div class="clearfix"> </div>
	</div>	
	<div id="small-dialog5" style="width: 20%" class="mfp-hide">
		<h3>Videos</h3> 
		<div class="signup">
			<form method="post">
				<div style="margin-left: 5%">
					<?php
					$email = $_SESSION['user']['email'];
					$sql = "SELECT a.*, b.id_video FROM video a left join playlist_detail b on a.id = b.id_video and b.id_playlist = $id WHERE user_id = '$email'";
					$query = $conn -> query($sql);
					while ($row = $query -> fetch_array()) {
						$checked = "";
						if ($row['id_video']) {
							$checked = "checked";
						}
						?>
						<div class="col-md-10 resent-grid recommended-grid sports-recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<img width="100%" src="<?php echo $row['thub']?>" alt="" />
								<div class="time small-time sports-tome">
									<p><?php echo duration($row['duration'])?></p>
								</div>
								<div class="clck sports-clock">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>
							</div>
							<div class="resent-grid-info recommended-grid-info">
								<h5><?php echo $row['name']?></h5>
								<p class="views"><?php formatViews($row['views'])?> views</p>
							</div>
						</div>
						<input type="checkbox" name="<?php echo $row['id']?>" <?php echo $checked?> />
						<div class="clearfix"> </div>
						<?php
					}
					?>
				</div>
				<input type="submit" name="update-video-list"  value="UPDATE"/>
			</form>
		</div>
		<div class="clearfix"> </div>
	</div>	
</div>
<div class="clearfix"> </div>